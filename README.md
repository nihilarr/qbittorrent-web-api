
# qbittorrent-web-api

> Library to access qBittorrent using the webui

## Requirements

* [PHP](https://secure.php.net/manual/en/install.php) >= 5.3.29

## Install

### Composer
```bash
composer require nihilarr/qbittorrent-web-api
```
### Non-Composer
```php
require('QBittorrentWebApi.php'); // Require QBittorrentWebApi/QBittorrentWebApi.php file
```

## Usage

```php
use Nihilarr\QBittorrentWebApi;

$qbt = new QBittorrentWebApi('localhost', 8080, 'admin', 'adminadmin');

if($qbt->login()) {
    $torrents = $qbt->torrents();

    var_dump($torrents);
}

# Array
# (
#   [0] =>
#     object(stdClass)#2 (35) {
#       ["added_on"] => int(1526945166)
#       ["amount_left"] => int(0)
#       ["category"] => string(6) "movies"
#       ["completed"] => int(3410)
#       ["completion_on"] => int(1526945167)
#       ["dl_limit"] => int(-1)
#       ["dlspeed"] => int(0)
#       ["downloaded"] => int(0)
#       ["downloaded_session"] => int(0)
#       ["eta"] => int(8640000)
#       ["f_l_piece_prio"] => bool(false)
#       ["force_start"] => bool(false)
#       ["hash"] => string(40) "a7ac9dd80ab77b3b60a6bea2a0ccb601f00f7cd4"
#       ["last_activity"] => int(1527451713)
#       ["name"] => string(54) "Resident.Evil.Retribution.2012.720p.BRRip.x264-testing"
#       ["num_complete"] => int(0)
#       ["num_incomplete"] => int(2)
#       ["num_leechs"] => int(0)
#       ["num_seeds"] => int(0)
#       ["priority"] => int(0)
#       ["progress"] => int(1)
#       ["ratio"] => int(0)
#       ["ratio_limit"] => int(-1)
#       ["save_path"] => string(24) "/path/to/Downloads/"
#       ["seen_complete"] => int(4294967295)
#       ["seq_dl"] => bool(false)
#       ["size"] => int(353410)
#       ["state"] => string(9) "downloading"
#       ["super_seeding"] => bool(false)
#       ["total_size"] => int(353410)
#       ["tracker"] => string(0) ""
#       ["up_limit"] => int(-1)
#       ["uploaded"] => int(0)
#       ["uploaded_session"] => int(0)
#       ["upspeed"] => int(0)
#     }
# )
```

### Note

## Contributing

## License

MIT © [Drew Smith](https://www.nihilarr.com)
