<?php
/**
 * QBittorrentWebApi
 *
 * Simple library to interact with a qBittorrent server via webui.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   QBittorrentWebApi
 * @author    Drew Smith
 * @copyright copyright (c) 2018, Nihilarr (https://www.nihilarr.com)
 * @license	  http://opensource.org/licenses/MIT	MIT License
 * @link      https://gitlab.com/nihilarr/qbittorrent-web-api
 * @version   0.0.1
 */

namespace Nihilarr;

/**
 * QBittorrentWebApi library
 */
class QBittorrentWebApi {
    /**
     * qBittorrent Host
     * @var string
     */
    protected $host = 'localhost';

    /**
     * qBittorrent Port
     * @var int
     */
    protected $port = '8080';

    /**
     * qBittorrent Username
     * @var string
     */
    protected $user = 'admin';

    /**
     * qBittorrent Password
     * @var string
     */
    protected $pass = 'adminadmin';

    /**
     * Debugging Status
     * @var bool
     */
    protected $debug = false;

    /**
     * qBittorrent cURL Session
     * @var resource
     */
    protected $session;

    /**
     * qBittorrent Session ID
     * @var string
     */
    protected $session_id;

    /**
     * Authentication Status
     * @var bool
     */
    protected $authenticated = false;

    /**
     * qBittorrent Preferences
     * @var array
     */
    protected $preferences;

    /**
     * Preference Changes
     * @var array
     */
    protected $changes;

    /**
     * Torrent Filters
     * @var array
     */
    protected $torrent_filters = array(
        'all',
        'downloading',
        'completed',
        'paused',
        'active',
        'inactive'
    );

    /**
     * Lob Level Normal
     * @var integer
     */
    const LOG_NORMAL = 1;

    /**
     * Log Level Info
     * @var integer
     */
    const LOG_INFO = 2;

    /**
     * Log Level Warning
     * @var integer
     */
    const LOG_WARNING = 4;

    /**
     * Log Level Critical
     * @var integer
     */
    const LOG_CRITICAL = 8;

    /**
     * Log Level All
     * @var integer
     */
    const LOG_ALL = 16;

    /**
     * Constructor
     * @param string $host qBittorrent host
     * @param integer $port qBittorrent port
     * @param string $user qBittorrent username
     * @param string $pass qBittorrent password
     */
    public function __construct(string $host = 'localhost', int $port = 8080, string $user = 'admin', string $pass = 'adminadmin') {
        // Set response code byte for PHP < 5.5.0
        if(!defined('CURLINFO_RESPONSE_CODE'))
            define('CURLINFO_RESPONSE_CODE', CURLINFO_HTTP_CODE);

        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;

        $this->session = curl_init();
        curl_setopt($this->session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->session, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->session, CURLOPT_TIMEOUT, 400);
    }

    /**
     * Destructor
     */
    public function __destruct() {
        $this->logout();
        curl_close($this->session);
    }

    /**
     * Getter. Used to get qBittorrent preferences.
     * @param string $key Preference name
     * @return mixed Value of preference or void if $key not found
     */
    public function __get(string $key) {
        if(isset($this->preferences[$key]))
            return $this->preferences[$key]['value'];
    }

    /**
     * Setter. Used to modify qBittorrent preferences.
     * @param string $key Preference name
     * @param mixed $value Value to set $key preference to
     */
    public function __set(string $key, $value) {
        if(!isset($this->preferences[$key]))
            trigger_error("Preference '{$key}' does not exist.", E_USER_ERROR);

        $valid_type = $this->preferences[$key]['type'];
        $given_type = gettype($value);

        if($valid_type != $given_type)
            trigger_error("Preference '{$key}' must be of the type {$valid_type}, {$given_type} given.", E_USER_ERROR);

        $this->changes[$key] = $value;
    }

    /**
     * Set or get qBittorrent host.
     * @param string $host qBittorrent host
     * @return mixed Instance of QBittorrentWebApi, current host if $host is null
     */
    public function host(string $host = null) {
        if(null !== $host) {
            $this->host = $host;
            return $this;
        }
        return $this->host;
    }

    /**
     * Set or get qBittorrent port.
     * @param int $port qBittorrent port
     * @return mixed Instance of QBittorrentWebApi, current port if $port is null
     */
    public function port(int $port) {
        if(null !== $port) {
            $this->port = $port;
            return $this;
        }
        return $this->port;
    }

    /**
     * Set or get qBittorrent username.
     * @param string $user qBittorrent username
     * @return mixed Instance of QBittorrentWebApi, current user if $user is null
     */
    public function user(string $user) {
        if(null !== $user) {
            $this->user = $user;
            return $this;
        }
        return $this->user;
    }

    /**
     * Set qBittorrent password.
     * @param  string $pass qBittorrent password
     * @return object Instance of QBittorrentWebApi
     */
    public function pass(string $pass) {
        $this->pass = $pass;
        return $this;
    }

    /**
     * Set or get debugging status.
     * @param bool $debug True to enable debugging, false to disable
     * @return mixed Instance of QBittorrentWebApi, current status if $debug is null
     */
    public function debug(bool $debug = null) {
        if(null !== $debug) {
            $this->debug = $debug;
            return $this;
        }
        return $this->debug;
    }

    /**
     * qBittorrent authentication status
     * @return bool True if authenticated, false if not
     */
    public function authenticated() {
        return $this->authenticated;
    }

    /**
     * qBittorrent API version.
     * @return string API version
     */
    public function api_version() {
        return $this->request('GET', 'version/api');
    }

    /**
     * qBittorrent minimum API version.
     * @return string API Version
     */
    public function api_min() {
        return $this->request('GET', 'version/api_min');
    }

    /**
     * qBittorrent version.
     * @return string Version
     */
    public function qbittorrent_version() {
        return $this->request('GET', 'version/qbittorrent');
    }

    /**
     * Attempt to authenticate against qBittorrent client.
     * @return bool True if authentication was successful, false if not
     */
    public function login(bool $autoload_preferences = false) {
        if(!$this->authenticated) {
            $this->authenticated = !!$this->request('POST', 'login', array(
                'username' => $this->user,
                'password' => $this->pass
            ));

            // Autoload preferences if login successful
            if($this->authenticated && $autoload_preferences) {
                $this->preferences();
            }
        }
        return $this->authenticated;
    }

    /**
     * Destroy qBittorrent session.
     * @return bool True on success, false on failure
     */
    public function logout() {
        if($this->authenticated) {
            $this->request('POST', 'logout');
            $this->authenticated = false;
            $this->session_id = null;
            $this->preferences = null;
            $this->changes = null;
            return true;
        }
        return false;
    }

    /**
     * Shutdown qBittorrent client.
     * @return bool True on success, false on failure
     */
    public function shutdown() {
        return !!$this->request('GET', 'command/shutdown');
    }

    /**
     * Load qBittorrent preferences.
     * @param bool $force_reload Force reload preferences
     * @return void
     */
    public function preferences(bool $force_reload = false) {
        if(null !== $this->preferences && !$force_reload)
            return $this->preferences;

        $preferences = $this->request('GET', 'query/preferences');
        if( count($preferences) > 0) {
            foreach($preferences as $preference => $value) {
                $this->preferences[$preference] = array(
                    'value' => $value,
                    'type' => gettype($value)
                );
            }
        }
        return $this->preferences;
    }

    /**
     * Get torrents.
     * @param array $params Optional filters
     * @return array Array of torrents
     */
    public function torrents(array $params = array()) {
        return $this->request('GET', 'query/torrents', $params);
    }

    /**
     * Get torrent properties.
     * @param string $torrent_hash Torrent hash
     * @return mixed Torrent object or false if torrent doesn't exist
     */
    public function torrent_generic_properties(string $torrent_hash) {
        return $this->request('GET', "query/propertiesGeneral/{$torrent_hash}");
    }

    /**
     * Get torrent trackers.
     * @param string $torrent_hash Torrent hash
     * @return mixed Array of torrent tracker objects, false if torrent doesn't exist
     */
    public function torrent_trackers(string $torrent_hash) {
        return $this->request('GET', "query/propertiesTrackers/{$torrent_hash}");
    }

    /**
     * Get torrent web seeds.
     * @param  string $torrent_hash Torrent hash
     * @return mixed Array of web seed objects, false if torrent doesn't exist
     */
    public function torrent_web_seeds(string $torrent_hash) {
        return $this->request('GET', "query/propertiesWebSeeds/{$torrent_hash}");
    }

    /**
     * Get contents of a torrent.
     * @param string $torrent_hash Torrent hash
     * @return array Array of torrent content objects
     */
    public function torrent_contents(string $torrent_hash) {
        return $this->request('GET', "query/propertiesFiles/{$torrent_hash}");
    }

    /**
     * Get states (integers) of all pieces (in order) of a torrent.
     * @param string $torrent_hash Torrent hash
     * @return array Array of states
     */
    public function torrent_piece_states(string $torrent_hash) {
        return $this->request('GET', "query/getPieceStates/{$torrent_hash}");
    }

    /**
     * Get hashes (strings) of all pieces (in order) of a torrent.
     * @param string $torrent_hash Torrent hash
     * @return array Array of hashes
     */
    public function torrent_piece_hashes(string $torrent_hash) {
        return $this->request('GET', "query/getPieceHashes/{$torrent_hash}");
    }

    /**
     * Pause torrent.
     * @param string $torrent_hash Torrent hash
     * @return bool True on success, false on failure
     */
    public function torrent_pause(string $torrent_hash) {
        $params = array('hash' => $torrent_hash);
        return !!$this->request('POST', "command/pause", $params);
    }

    /**
     * Resume torrent.
     * @param string $torrent_hash Torrent hash
     * @return bool True on success, false on failure
     */
    public function torrent_resume(string $torrent_hash) {
        $params = array('hash' => $torrent_hash);
        return !!$this->request('POST', "command/resume", $params);
    }

    /**
     * Pause all torrents.
     * @return bool True on success, false on failure
     */
    public function torrent_pause_all() {
        return !!$this->request('POST', "command/pauseAll");
    }

    /**
     * Resume all torrents.
     * @return bool True on success, false on failure
     */
    public function torrent_resume_all() {
        return !!$this->request('POST', "command/resumeAll");
    }

    /**
     * Delete torrent(s).
     * @param string|array $torrent_hash Torrent hash(es)
     * @param boolean $with_data True to remove torrent with data, false to remove only torrent
     * @return bool True on success, false on failure
     */
    public function torrent_delete($torrent_hash, bool $with_data = false) {
        $command = $with_data ? 'deletePerm' : 'delete';
        $params = array('hashes' => $this->hashes($torrent_hash));
        return !!$this->request('POST', "command/{$command}", $params);
    }

    /**
     * Increase torrent(s) priority.
     * @param string|array $torrent_hash Torrent hash(es)
     * @return bool True on success, false on failure
     */
    public function torrent_increase_priority($torrent_hash) {
        $params = array('hashes' => $this->hashes($torrent_hash));
        return !!$this->request('POST', "command/increasePrio", $params);
    }

    /**
     * Decrease torrent(s) priority.
     * @param string|array $torrent_hash Torrent hash(es)
     * @return bool True on success, false on failure
     */
    public function torrent_decrease_priority($torrent_hash) {
        $params = array('hashes' => $this->hashes($torrent_hash));
        return !!$this->request('POST', "command/decreasePrio", $params);
    }

    /**
     * Maximize torrent(s) priority.
     * @param string|array $torrent_hash Torrent hash(es)
     * @return bool True on success, false on failure
     */
    public function torrent_max_priority($torrent_hash) {
        $params = array('hashes' => $this->hashes($torrent_hash));
        return !!$this->request('POST', "command/topPrio", $params);
    }

    /**
     * Minimize torrent(s) priority.
     * @param string|array $torrent_hash Torrent hash(es)
     * @return bool True on success, false on failure
     */
    public function torrent_min_priority($torrent_hash) {
        $params = array('hashes' => $this->hashes($torrent_hash));
        return !!$this->request('POST', "command/bottomPrio", $params);
    }

    /**
     * Set torrent file priority.
     * @param string $torrent_hash Torrent hash
     * @param int $file_id ID on file in torrent
     * @param int $priority File priority
     * @return bool True on success, false on failure
     */
    public function torrent_file_priority(string $torrent_hash, int $file_id, int $priority) {
        $params = array(
            'hash' => $torrent_hash,
            'id' => $file_id,
            'priority' => $priority
        );
        return !!$this->request('POST', "command/setFilePrio", $params);
    }

    /**
     * Set or get torrent download limit.
     * @param string|array $torrent_hash Torrent hash(es)
     * @param int $limit Limit in bytes
     * @return mixed True on success, current download limit if $limit is null
     */
    public function torrent_download_limit($torrent_hash, int $limit = null) {
        if(null === $limit) {
            $command = 'getTorrentsDlLimit';
            $params = array();
        }
        else {
            $command = 'setTorrentsDlLimit';
            $params = array(
                'hashes' => $this->hashes($torrent_hash),
                'limit' => $limit
            );
        }
        return $this->request('POST', "command/{$command}", $params);
    }

    /**
     * Set or get torrent upload limit.
     * @param string|array $torrent_hash Torrent hash(es)
     * @param int $limit Limit in bytes
     * @return mixed True on success, current upload limit if $limit is null
     */
    public function torrent_upload_limit($torrent_hash, int $limit = null) {
        if(null === $limit) {
            $command = 'getTorrentsUpLimit';
            $params = array();
        }
        else {
            $command = 'setTorrentsUpLimit';
            $params = array(
                'hashes' => $this->hashes($torrent_hash),
                'limit' => $limit
            );
        }
        return $this->request('POST', "command/{$command}", $params);
    }

    /**
     * Set or get global download limit.
     * @param int $limit Limit in bytes
     * @return mixed True on success, current download limit if $limit is null
     */
    public function global_download_limit(int $limit = null) {
        if(null === $limit) {
            $command = 'getGlobalDlLimit';
            $params = array();
        }
        else {
            $command = 'setGlobalDlLimit';
            $params = array('limit' => $limit);
        }
        return $this->request('POST', "command/{$command}", $params);
    }

    /**
     * Set or get global upload limit.
     * @param int $limit Limit in bytes
     * @return mixed True on success, current upload limit if $limit is null
     */
    public function global_upload_limit(int $limit = null) {
        if(null === $limit) {
            $command = 'getGlobalUpLimit';
            $params = array();
        }
        else {
            $command = 'setGlobalUpLimit';
            $params = array('limit' => $limit);
        }
        return $this->request('POST', "command/{$command}", $params);
    }

    /**
     * Get info you usually see in qBittorrent status bar.
     * @return object Transfer information
     */
    public function global_transfer_info() {
        return $this->request('GET', 'query/transferInfo');
    }

    /**
     * Get changes since the last request.
     * @param integer $response_id Response ID
     * @return object Changes since last request
     */
    public function partial_data(int $response_id = 0) {
        return $this->request('GET', 'sync/maindata', array('rid' => $response_id));
    }

    /**
     * Get log messages.
     * @param int $log_level Included messages (normal, info, warning, critical)
     * @param int $last_known_id Last known message ID
     * @return array Array of log objects
     */
    public function get_log(int $log_level = self::LOG_ALL, int $last_known_id = -1) {
        $params = array();
        if($log_level & self::LOG_ALL) {
            $params['normal'] = true;
            $params['info'] = true;
            $params['warning'] = true;
            $params['critical'] = true;
        }
        else {
            $params['normal'] = !!($log_level & self::LOG_NORMAL);
            $params['info'] = !!($log_level & self::LOG_INFO);
            $params['warning'] = !!($log_level & self::LOG_WARNING);
            $params['critical'] = !!($log_level & self::LOG_CRITICAL);
        }
        $params['last_known_id'] = $last_known_id;
        return $this->request('GET', 'query/getLog', $params);
    }

    /**
     * Get the state of the alternative speed limits.
     * @return bool True if enabled, false if not
     */
    public function alt_speed_limits_state() {
        return !!$this->request('POST', 'command/alternativeSpeedLimitsEnabled');
    }

    /**
     * Toggle the state of the alternative speed limits.
     * @return bool bool True if enabled, false if not
     */
    public function alt_speed_limits_toggle() {
        return !!$this->request('POST', 'command/toggleAlternativeSpeedLimits');
    }

    /**
     * Toggle the state of sequential downloads.
     * @param string|array $torrent_hash Torrent hash(es)
     * @return bool True if enabled, false if not
     */
    public function sequential_download_toggle($torrent_hash) {
        $params = array('hashes' => $this->hashes($torrent_hash));
        return !!$this->request('POST', 'command/toggleSequentialDownload', $params);
    }

    /**
     * Toggle first/last piece priority.
     * @param string|array $torrent_hash Torrent hash(es)
     * @return bool True if enabled, false if not
     */
    public function fl_piece_priority_toggle($torrent_hash) {
        $params = array('hashes' => $this->hashes($torrent_hash));
        return !!$this->request('POST', 'command/toggleFirstLastPiecePrio', $params);
    }

    /**
     * Force start a torrent.
     * @param string|array $torrent_hash Torrent hash(es)
     * @param bool $force_start True to force start, false to not
     * @return bool True if successful, false if not
     */
    public function force_start($torrent_hash, bool $force_start) {
        $params = array(
            'hashes' => $this->hashes($torrent_hash),
            'value' => $force_start
        );
        return !!$this->request('POST', 'command/setForceStart', $params);
    }

    /**
     * Super seed torrent(s).
     * @param string|array $torrent_hash Torrent hash(es)
     * @param bool $super_seed True to super seed, false to not
     * @return bool True if successful, false if not
     */
    public function super_seeding($torrent_hash, bool $super_seed) {
        $params = array(
            'hashes' => $this->hashes($torrent_hash),
            'value' => $super_seed
        );
        return !!$this->request('POST', 'command/setSuperSeeding', $params);
    }

    /**
     * Add torrent(s) to a category. Caategory will be created if it doesn't exist.
     * @param string|array $torrent_hash Torrent hash(es)
     * @param string $category Category name
     * @return bool True if successful, false if not
     */
    public function set_category($torrent_hash, string $category) {
        $params = array(
            'hashes' => $this->hashes($torrent_hash),
            'category' => $category
        );
        return !!$this->request('POST', 'command/setCategory', $params);
    }

    /**
     * Create a category.
     * @param string $category Category name
     * @return bool True if successful, false if not
     */
    public function add_category(string $category) {
        return !!$this->request('POST', 'command/addCategory', array('category' => $category));
    }

    /**
     * Remove a category.
     * @param string|array $categories Category name(s)
     * @return bool True if successful, false if not
     */
    public function remove_categories($categories) {
        if(is_array($categories))
            $categories = implode("\n", $categories);
        $params = array('categories' => $categories);
        return !!$this->request('POST', 'command/removeCategories', $params);
    }

    /**
     * Set torrent to be auto managed.
     * @param string|array $torrent_hash Torrent hash(es)
     * @param bool $enable True to auto manage, false to not
     * @return bool True if successful, false if not
     */
    public function torrent_auto_manage($torrent_hash, bool $enable) {
        $params = array(
            'hashes' => $this->hashes($torrent_hash),
            'enable' => $enable
        );
        return !!$this->request('POST', 'command/setAutoTMM', $params);
    }

    /**
     * Download location of torrent.
     * @param string|array $torrent_hash Torrent hash(es)
     * @param string $location Download path
     * @return bool True if successful, false if not
     */
    public function torrent_location($torrent_hash, string $location) {
        $params = array(
            'hashes' => $this->hashes($torrent_hash),
            'location' => $location
        );
        return !!$this->request('POST', 'command/setLocation', $params);
    }

    /**
     * Set a torrent name.
     * @param string $torrent_hash Torrent hash
     * @param string $name Torrent name
     * @return bool True if successful, false if not
     */
    public function torrent_name(string $torrent_hash, string $name) {
        $params = array(
            'hash' => $torrent_hash,
            'name' => $name
        );
        return !!$this->request('POST', 'command/rename', $params);
    }

    /**
     * Save changes to qBittorrent preferences.
     * @return bool True on success, false on failure
     */
    public function save() {
        $saved = false;
        if(null !== $this->changes) {
            $json = json_encode($this->changes);

            if(!!$this->request('POST', 'command/setPreferences', array('json' => $json))) {
                foreach($this->changes as $preference => $change) {
                    $this->preferences[$preference]['value'] = $change;
                }

                $this->reset();
                $saved = true;
            }
        }
        else {
            $saved = true;
        }
        return !!$saved;
    }

    /**
     * Resent preferences to original values.
     */
    public function reset() {
        $this->changes = null;
    }

    /**
     * Send request to qBittorrent.
     * @param string $method GET, POST
     * @param string $route API URL path
     * @param array $params Optional parameters
     * @return mixed
     */
    protected function request(string $method, string $route, array $params = array()) {
        $no_auth_routes = array('login', 'logout', 'version/api', 'version/api_min', 'version/qbittorrent');
        if(!$this->authenticated && !in_array($route, $no_auth_routes)) {
            trigger_error('You are not currently logged in', E_USER_ERROR);
        }

        $request_uri = "http://{$this->host}:{$this->port}/{$route}";
        $data = http_build_query($params, '', '&', PHP_QUERY_RFC3986);

        $method = strtoupper(trim($method));
        $headers = array('User-Agent: Fiddler');

        if($this->authenticated)
            $headers[] = "Cookie: SID={$this->session_id}";

        switch($method) {
            case 'POST':
                $headers[] = 'Content-Type: application/x-www-form-urlencoded';
                $headers[] = 'Content-Length: ' . strlen($data);

                curl_setopt($this->session, CURLOPT_POST, true);
                curl_setopt($this->session, CURLOPT_HEADER, ($route == 'login'));
                curl_setopt($this->session, CURLOPT_POSTFIELDS, $data);

                break;

            case 'GET':
                $headers[] = 'Accept: application/json';

                if(!empty($data))
                    $request_uri .= "?{$data}";

                curl_setopt($this->session, CURLOPT_POST, false);
                curl_setopt($this->session, CURLOPT_HEADER, false);
                curl_setopt($this->session, CURLOPT_POSTFIELDS, '');

                break;

            default:
                trigger_error("Invalid method type '{$method}' specified", E_USER_ERROR);
                break;
        }

        curl_setopt($this->session, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($this->session, CURLOPT_URL, $request_uri);
        $response = curl_exec($this->session);
        $error_code = curl_getinfo($this->session, CURLINFO_RESPONSE_CODE);

        if($error_code == 200) {
            if($route == 'login') {
                $this->session_id = $this->get_session_id($response);
                return ($this->session_id !== false);
            }
            else {
                if(!empty($response = trim($response))) {
                    return json_decode($response);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Grab session ID from auth attempt.
     * @param string $headers Auth request headers
     * @return string|bool Session ID, false on failure
     */
    protected function get_session_id(string $headers) {
        $session_id = false;
        if(($start = stripos($headers, 'SID=')) !== false) {
            $line = substr($headers, $start + 4);
            $session_id = substr($line, 0, strpos($line, ';'));
        }
        return $session_id;
    }

    /**
     * Generate hashes string.
     * @param array|string $hash Torrent hash(es)
     * @return string String of hashes seperated by pipe
     */
    private function hashes($hash) {
        return is_array($hash) ? implode('|', $hash) : (string)$hash;
    }

}
